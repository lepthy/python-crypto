from Crypto.Cipher import AES
from Crypto import Random

import time

import base64
import sys

class Timer():
	start_time = None
	name = None
	def __init__(self, name='root', output=sys.stderr):
		self.name = name
		self.output = output

	def reset(self):
		print("--- {}: reset ---".format(self.name), file=self.output)
		self.start_time = time.time()

	def print(self, output=sys.stdout):
		print("--- {}: {} seconds ---".format(self.name, time.time() - self.start_time), file=output)

class Cipher():
	key = None
	def __init__(self):
		self.key = Random.new().read(32)

	def encrypt(self, raw):
		iv = Random.new().read(AES.block_size)
		cipher = AES.new(self.key, AES.MODE_CFB, iv)
		payload = iv + cipher.encrypt(raw)
		return base64.b64encode(payload)

	def decrypt(self, enc):
		enc = base64.b64decode(enc)
		iv = enc[:AES.block_size]
		cipher = AES.new(self.key, AES.MODE_CFB, iv)
		return cipher.decrypt(enc[AES.block_size:])

def test(payload):
	c = Cipher()
	t.reset()
	enc = c.encrypt(payload)
	t.print()
	t.reset()
	msg = c.decrypt(enc)
	t.print()
	if payload == msg:
		print('success')
	else:
		print('failure')

def generate_payload(size):
	iv = Random.new().read(size)
	return base64.b64encode(iv)

t = Timer('crypto')

size = int(sys.argv[1])
message = generate_payload(size)
test(message)
